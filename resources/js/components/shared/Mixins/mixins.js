import axios from 'axios';

export default {
    components: {
        Modal: () => import('./../Modal/Modal')
    },
    data(){
        return{
            baseURL: process.env.MIX_BASE_URL,
        }
    },
    methods: {
        notification(title, text, type, duration = 2000) {
            this.$notify({
                group: 'submit',
                clean: true
            })
            this.$notify({
                group: 'submit',
                title: title,
                text: text,
                type: type,
                duration: duration,
            })
        },
        
        dataTable(name) {
            $(name).DataTable({
                pageLength: 50,
                "iDisplayLength": 50,
                "ordering": false,
                "retrieve": true,
                "language": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ Registros por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    }
                }
            });
        },

        async buscaCEP(cep) {
            cep = cep.replace('-', '');
            if (cep.length >= 8) {
               let response = await fetch(`https://viacep.com.br/ws/${cep}/json`)
               return await response.json();
            }else{
                return []
            }

        },

        async getestados(){
            let response = axios.get(`${process.env.MIX_BASE_URL}/get/estados`);
            return await response;
        },

        async getmunicipios(idestado){
            let response = axios.get(`${process.env.MIX_BASE_URL}/get/municipios/${idestado}`);
            return await response;
        },

        async getplanos(){
            let response = axios.get(`${process.env.MIX_BASE_URL}/get/planos`);
            return await response;
        },

        async getpacientes(){
            let response = axios.get(`${process.env.MIX_BASE_URL}/get/pacientes`);
            return await response;
        },

        openModal(name){
            $(name).modal('show');
        },

        closeModal(name){
            $(name).hide();
            $(".modal-backdrop").remove();

        }
    }
}