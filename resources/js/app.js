import VeeValidate, { Validator } from 'vee-validate'
import pt_BR from 'vee-validate/dist/locale/pt_BR'
import Notifications from 'vue-notification'
import VueMask from 'v-mask'
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
import moment from 'moment'

require('./bootstrap');
window.Vue = require('vue').default;

Vue.use(VeeValidate);
Vue.use(Notifications)
Validator.localize('pt_BR', pt_BR);
Vue.use(VueMask);
Vue.component('v-select', vSelect)

Vue.filter('momentFull', function (date) {
    return date ? moment(date).format('DD/MM/YYYY HH:mm') : null
})

Vue.filter('momentDate', function (date) {
    return date ? moment(date).format('DD/MM/YYYY') : null
});


const app = new Vue({
    el: '#app',
    components:{
        Index: () => import('./components/index/Index'),
        PacientePage: () => import('./components/paciente/PacientePage.vue'),

       
    }
});
