<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon 	d-sm-block d-md-none ">
            <img src="{{ asset('image/icon.fw.png') }}" alt="Logo SW DENTAL" class="img-fluid">
        </div>
        <div class="sidebar-brand-text mx-3 d-md-none d-lg-block">
            <img src="{{ asset('image/logo-swdental.fw.png') }}" alt="Logo SW DENTAL" class="img-fluid">
        </div>
    </a>

    <div class="sidebar-heading">
        Informações
    </div>

    <!-- Nav Item - Inteligência -->
    <li class="nav-item">
        <a class="nav-link" href="index.html">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Inteligência</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Coração do Sistema
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    {{-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Components</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Custom Components:</h6>
                <a class="collapse-item" href="buttons.html">Buttons</a>
                <a class="collapse-item" href="cards.html">Cards</a>
            </div>
        </div>
    </li> --}}

    <li class="nav-item {{ Request::is('pacientes') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('pacientes.index') }}">
            <i class="fas fa-users"></i> <span>Pacientes</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="index.html">
            <i class="fas fa-calendar"></i> <span>Agenda</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="index.html">
            <i class="fas fa-box"></i> <span>Estoque</span>
        </a>
    </li>

    <div class="sidebar-heading">
        Personalização
    </div>

    <li class="nav-item">
        <a class="nav-link" href="index.html">
            <i class="fas fa-cogs"></i> <span>Configurações</span>
        </a>
    </li>

</ul>
<!-- End of Sidebar -->