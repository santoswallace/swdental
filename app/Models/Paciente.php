<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Paciente extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'nome',
        'sexo',
        'data_nascimento',
        'cpf',
        'rg',
        'celular',
        'email',
        'telefone',
        'observacoes'
    ];

    public function plano(){
        return $this->hasOne(PacientePlano::class);
    }

    public function endereco(){
        return $this->hasOne(Endereco::class);
    }
}
