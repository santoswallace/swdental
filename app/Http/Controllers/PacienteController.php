<?php

namespace App\Http\Controllers;

use App\Models\Endereco;
use App\Models\Paciente;
use App\Models\PacientePlano;
use App\Models\Plano;
use App\Services\Logg;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PacienteController extends Controller
{
    public function index()
    {
        return view('app.paciente.index');
    }

    public function store(Request $request)
    {
        try {

            $post = $request->all();
            $post['data_nascimento'] = Carbon::createFromFormat('d/m/Y', $post['data_nascimento'])->format('Y-m-d');
            $paciente = Paciente::create($post);

            $post['endereco']['paciente_id'] = $paciente->id;
            $post['endereco']['municipio_id'] = $post['endereco']['municipio'];
            Endereco::create($post['endereco']);

            $post['plano']['paciente_id'] = $paciente->id;
            PacientePlano::create($post['plano']);

            Log::info('Criou Paciente | Request: ' . json_encode($request->all()));
            return response()->json($paciente, 201);
        } catch (Exception $e) {
            Logg::create($e, $request->all());
            return response()->json('Server Error', 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $post = $request->all();
            $paciente = Paciente::findOrfail($id);
            $post['data_nascimento'] = Carbon::createFromFormat('d/m/Y', $post['data_nascimento'])->format('Y-m-d');
            $paciente->update($post);
            $paciente->save();

            $post['endereco']['municipio_id'] = $post['endereco']['municipio'];
            $endereco = Endereco::findOrfail($post['endereco']['id']);
            $endereco->update($post['endereco']);
            $endereco->save();

            $plano = Plano::findOrfail($post['plano']['id']);
            $plano->update($post['plano']);
            $plano->save();

            Log::info('Update Paciente | Request: ' . json_encode($request->all()));
            return response()->json($paciente, 200);
        } catch (Exception $e) {
            Logg::create($e, $request->all());
            return response()->json('Server Error', 500);
        }
    }
}
