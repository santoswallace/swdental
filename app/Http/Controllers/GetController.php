<?php

namespace App\Http\Controllers;

use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Paciente;
use App\Models\Plano;
use Illuminate\Http\Request;

class GetController extends Controller
{
    public function getEstados($id = null)
    {
        return Estado::when($id, function($q) use ($id) {
            $q->whereId($id);
        })->get();
    }

    public function getMunicipios($estado_id)
    {
        return Municipio::when($estado_id, function($q) use ($estado_id) {
            $q->whereEstadoId($estado_id);
        })->get();
    }


    public function getPlanos($id = null)
    {
        return Plano::when($id, function($q) use ($id) {
            $q->whereId($id);
        })->get();
    }

    public function getPacientes($id = null)
    {
        return Paciente::with('plano', 'endereco', 'endereco.municipio:id,estado_id', 'endereco.municipio.estado:id,nome,sigla')->when($id, function($q) use ($id) {
            $q->whereId($id);
        })->get();
    }
}
