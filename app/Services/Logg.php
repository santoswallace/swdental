<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Logg
{
    public static function create($e, $request = [], $type = null)
    {   
        if(!$type){
            Log::error('ERROR: ' . $e . ' | ARQUIVO: ' . $e->getFile() . ' | LINHA: ' . $e->getLine() . ' | CÓDIGO: ' . $e->getCode() . ' | REQUEST: ' . json_encode($request));
        } 
    }

    public function __clone()
    {
        throw new Exception('Error: classe não instanciável');
    }
}
