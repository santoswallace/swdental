<?php

use App\Http\Controllers\GetController;
use App\Http\Controllers\PacienteController;
use App\Models\Paciente;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app.index.index');
});

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('pacientes', PacienteController::class);
Route::group(['prefix' => 'pacientes'], function(){
    Route::get('', [PacienteController::class, 'index'])->name('pacientes.index');
}); 

Route::group(['prefix' => 'get'], function(){
    Route::get('estados/{id?}', [GetController::class, 'getEstados'])->name('get.estado');
    Route::get('municipios/{estado_id}', [GetController::class, 'getMunicipios'])->name('get.municipios');
    Route::get('planos/{plano_id?}', [GetController::class, 'getPlanos'])->name('get.planos');
    Route::get('pacientes/{paciente_id?}', [GetController::class, 'getPacientes'])->name('get.pacientes');
});
